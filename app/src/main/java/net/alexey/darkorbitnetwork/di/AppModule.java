package net.alexey.darkorbitnetwork.di;


import com.squareup.otto.Bus;

import net.alexey.darkorbitnetwork.app.App;
import net.alexey.darkorbitnetwork.app.Preferences;
import net.alexey.darkorbitnetwork.network.Network;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Bus provideBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    public Network provideNetwork() {
        return new Network();
    }

    @Provides
    @Singleton
    public Preferences providePreferences() {
        return new Preferences(app);
    }
}

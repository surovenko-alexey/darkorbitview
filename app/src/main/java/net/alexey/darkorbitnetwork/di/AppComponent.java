package net.alexey.darkorbitnetwork.di;

import net.alexey.darkorbitnetwork.ui.base.BaseActivity;
import net.alexey.darkorbitnetwork.ui.base.BaseFragment;
import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(BaseActivity activity);

    void inject(BaseFragment fragment);

}
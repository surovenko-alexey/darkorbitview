package net.alexey.darkorbitnetwork.ui.user_screen.skilab;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import net.alexey.darkorbitnetwork.R;
import net.alexey.darkorbitnetwork.network.models.Skilab;
import net.alexey.darkorbitnetwork.network.models.UserData;
import net.alexey.darkorbitnetwork.ui.base.BaseFragment;
import net.alexey.darkorbitnetwork.ui.user_screen.UserActivity;

public class SkilabFragment extends BaseFragment {
    private UserData userData;

    public static SkilabFragment newInstance(String userData) {
        SkilabFragment fragment = new SkilabFragment();
        Bundle args = new Bundle();
        args.putString(UserActivity.USERDATA, userData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_skilab, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        showProgressBar(R.string.wait);
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userData = new Gson().fromJson(getArguments().getString(UserActivity.USERDATA), UserData.class);
        }
        getNetwork().parseSkilab().subscribe(this::completeParseSkilab, this::error);
    }

    private void completeParseSkilab(Skilab skilab) {
        
    }

    private void error(Throwable throwable) {
        showSnackBar(R.string.error);
    }
}

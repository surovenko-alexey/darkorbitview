package net.alexey.darkorbitnetwork.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;

import com.squareup.otto.Bus;

import net.alexey.darkorbitnetwork.app.App;
import net.alexey.darkorbitnetwork.app.Preferences;
import net.alexey.darkorbitnetwork.network.Network;

import javax.inject.Inject;

import butterknife.ButterKnife;
import icepick.Icepick;

public abstract class BaseFragment extends Fragment {

    @Inject
    Network network;

    @Inject
    Bus bus;

    @Inject
    Preferences preferences;

    private ProgressDialog progressDialog = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApp(this).getAppComponent().inject(this);
        if (savedInstanceState != null) {
            Icepick.restoreInstanceState(this, savedInstanceState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.unregister(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public Bus getBus() {
        return bus;
    }

    protected Preferences getPreferences() {
        return preferences;
    }

    public Network getNetwork() {
        return network;
    }

    public void showProgressBar(int textProgressBarId) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(textProgressBarId));
        progressDialog.show();
    }

    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    public void showSnackBar(int messageId) {
        View view = getView();
        if (view != null) {
            Snackbar snackbar = Snackbar.make(view, getString(messageId), Snackbar.LENGTH_LONG);
            snackbar.getView().setPadding(0, 0, 0, 0);
            snackbar.show();
        }
    }

}
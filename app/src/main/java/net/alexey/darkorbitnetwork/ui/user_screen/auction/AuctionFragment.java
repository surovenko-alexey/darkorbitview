package net.alexey.darkorbitnetwork.ui.user_screen.auction;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import net.alexey.darkorbitnetwork.R;
import net.alexey.darkorbitnetwork.network.models.Auction;
import net.alexey.darkorbitnetwork.network.models.UserData;
import net.alexey.darkorbitnetwork.ui.base.BaseFragment;
import net.alexey.darkorbitnetwork.ui.user_screen.UserActivity;

import org.jsoup.Connection;

import java.io.IOException;

import butterknife.Bind;


public class AuctionFragment extends BaseFragment {
    private UserData userData;

    @Bind(R.id.auction_recycler_view)
    RecyclerView auctionRecyclerView;

    public static AuctionFragment newInstance(String userData) {
        AuctionFragment fragment = new AuctionFragment();
        Bundle args = new Bundle();
        args.putString(UserActivity.USERDATA, userData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgressBar(R.string.wait);
        if (getArguments() != null) {
            userData = new Gson().fromJson(getArguments().getString(UserActivity.USERDATA), UserData.class);
        }
        getNetwork().parseAuction().subscribe(this::completeParseAuction, this::error);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializationRecyclerView();
    }

    private void initializationRecyclerView() {
        RecyclerView.LayoutManager auctionLayoutManager = new LinearLayoutManager(getActivity());
        auctionRecyclerView.setLayoutManager(auctionLayoutManager);
        auctionRecyclerView.setHasFixedSize(false);
    }

    public void completePasteRate(Connection.Response response) {
        try {
            getNetwork().parseAuction(response.parse()).subscribe(this::completeParseAuction, this::error);
        } catch (IOException e) {
            error(e);
        }
    }

    private void completeParseAuction(Auction auction) {
        RecyclerView.Adapter auctionItemAdapter = new AuctionItemAdapter(this, auction);
        auctionRecyclerView.setAdapter(auctionItemAdapter);
        hideProgressBar();
        if (auction.getMessage() != null && !auction.getMessage().isEmpty()) {
            final AlertDialog auctionMessageDialog = new AlertDialog.Builder(getContext())
                    .setMessage(auction.getMessage())
                    .setPositiveButton(R.string.ok, (dialog, which) -> {
                        dialog.cancel();
                    }).create();
            auctionMessageDialog.show();
        }
    }


    private void error(Throwable throwable) {
        hideProgressBar();
        showSnackBar(R.string.error);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_auction, container, false);
    }

}

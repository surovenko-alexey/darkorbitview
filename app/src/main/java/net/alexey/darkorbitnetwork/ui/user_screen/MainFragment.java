package net.alexey.darkorbitnetwork.ui.user_screen;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import net.alexey.darkorbitnetwork.R;
import net.alexey.darkorbitnetwork.network.models.UserData;
import net.alexey.darkorbitnetwork.ui.base.BaseFragment;

import butterknife.Bind;


public class MainFragment extends BaseFragment {
    private UserData userData;

    @Bind(R.id.user_info)
    TextView userInfo;


    public static MainFragment newInstance(String userData) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(UserActivity.USERDATA, userData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userData = new Gson().fromJson(getArguments().getString(UserActivity.USERDATA), UserData.class);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        StringBuilder builder = new StringBuilder("Ник: ")
                .append(userData.getNickname())
                .append("\nУровень: ")
                .append(userData.getLvl())
                .append("\nОпыт: ")
                .append(userData.getExperience())
                .append("\nЧесть: ")
                .append(userData.getHonor())
                .append("\nКомпания: ")
                .append(userData.getCompany())
                .append("\nЗвание: ")
                .append(userData.getRank())
                .append("\nПозиция: ")
                .append(userData.getPosition())
                .append("\nРанг: ")
                .append(userData.getPoint());
        userInfo.setText(builder.toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

}

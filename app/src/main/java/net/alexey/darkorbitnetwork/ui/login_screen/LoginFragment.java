package net.alexey.darkorbitnetwork.ui.login_screen;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import net.alexey.darkorbitnetwork.R;
import net.alexey.darkorbitnetwork.network.models.UserData;
import net.alexey.darkorbitnetwork.ui.base.BaseFragment;

import org.jsoup.Connection;

import java.io.IOException;

import butterknife.Bind;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment {

    @Bind(R.id.loginText)
    EditText loginText;

    @Bind(R.id.passText)
    EditText passText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    private String getEditText(EditText editText, String errorMessage) {
        if (editText.getText().toString().length() < 4) {
            editText.setError(errorMessage);
            return null;
        } else {
            return editText.getText().toString();
        }
    }

    @OnClick(R.id.loginBtn)
    public void loginBtnClick() {
        String login = getEditText(loginText, getResources().getString(R.string.field_can_not_be_empty));
        String pass = getEditText(passText, getResources().getString(R.string.field_can_not_be_empty));
        if (login != null && pass != null) {
            showProgressBar(R.string.wait);
            getNetwork().login(login, pass).subscribe(this::completeLogin, this::error);
        }
    }

    private void completeLogin(Connection.Response response) {
        if (response.url().toString().contains("internalStart")) {
            try {
                getNetwork().setCookie(response.cookies());
                getNetwork().parseUserInfo(response.parse()).subscribe(this::completeParseUserInfo, this::error);
            } catch (IOException e) {
                showSnackBar(R.string.error);
                hideProgressBar();
            }
        } else {
            showSnackBar(R.string.wrong_data);
            hideProgressBar();
        }
    }

    private void completeParseUserInfo(UserData userData) {
        getBus().post(userData);
        hideProgressBar();
    }


    private void error(Throwable throwable) {
        hideProgressBar();
        showSnackBar(R.string.error);
    }


}

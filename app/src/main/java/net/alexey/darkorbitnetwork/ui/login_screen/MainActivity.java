package net.alexey.darkorbitnetwork.ui.login_screen;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import net.alexey.darkorbitnetwork.R;
import net.alexey.darkorbitnetwork.network.models.UserData;
import net.alexey.darkorbitnetwork.ui.user_screen.UserActivity;
import net.alexey.darkorbitnetwork.ui.base.BaseActivity;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        replaceFragment(new LoginFragment());
    }

    @Subscribe
    public void openUserFragment(UserData event) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.putExtra(UserActivity.USERDATA, new Gson().toJson(event));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}

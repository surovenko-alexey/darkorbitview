package net.alexey.darkorbitnetwork.ui.user_screen;

import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import net.alexey.darkorbitnetwork.R;
import net.alexey.darkorbitnetwork.network.models.UserData;
import net.alexey.darkorbitnetwork.ui.base.BaseActivity;
import net.alexey.darkorbitnetwork.ui.user_screen.auction.AuctionFragment;
import net.alexey.darkorbitnetwork.ui.user_screen.skilab.SkilabFragment;


public class UserActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public final static String USERDATA = "USERDATA";
    private UserData userData;

    View headerView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        userData = new Gson().fromJson(getIntent().getStringExtra(USERDATA), UserData.class);
        loadNavigationViewData(navigationView);
        replaceFragment(MainFragment.newInstance(getIntent().getStringExtra(USERDATA)));
    }

    private void loadNavigationViewData(NavigationView navigationView) {
        headerView = navigationView.getHeaderView(0);
        ((TextView) headerView.findViewById(R.id.nickname)).setText(userData.getNickname());
        ((TextView) headerView.findViewById(R.id.money))
                .setText(new StringBuilder("CR: ")
                        .append(userData.getCredits())
                        .append("  URI: ")
                        .append(userData.getUridium())
                        .toString());
        ImageView rank = (ImageView) headerView.findViewById(R.id.rank);
        ImageView avatar = (ImageView) headerView.findViewById(R.id.avatar);
        Picasso.with(this).load(userData.getRankUrl()).into(rank);
        Picasso.with(this).load(userData.getAvatarUrl()).into(avatar);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.main) {
            replaceFragment(MainFragment.newInstance(getIntent().getStringExtra(USERDATA)));
        } else if (id == R.id.auction) {
            replaceFragment(AuctionFragment.newInstance(getIntent().getStringExtra(USERDATA)));
        } else if (id == R.id.skilab) {
            replaceFragment(SkilabFragment.newInstance(getIntent().getStringExtra(USERDATA)));
        } else if (id == R.id.highteck) {
            System.out.println("HIGHTTTA");
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

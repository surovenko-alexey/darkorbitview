package net.alexey.darkorbitnetwork.ui.user_screen.auction;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import net.alexey.darkorbitnetwork.R;
import net.alexey.darkorbitnetwork.network.models.Auction;
import net.alexey.darkorbitnetwork.network.models.AuctionItem;
import net.alexey.darkorbitnetwork.ui.base.BaseFragment;

import org.jsoup.Connection;

public class AuctionItemAdapter extends RecyclerView.Adapter {
    BaseFragment baseFragment;
    Auction auction;

    public AuctionItemAdapter(BaseFragment baseFragment, Auction auction) {
        this.baseFragment = baseFragment;
        this.auction = auction;
    }

    @Override
    public AuctionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AuctionItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.auction_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        AuctionItemViewHolder myHolder = (AuctionItemViewHolder) holder;
        AuctionItem auctionItem = auction.getAuctionItems().get(position);
        Picasso.with(baseFragment.getContext()).load(auctionItem.getImgUrl()).into(myHolder.image);
        myHolder.name.setText(auctionItem.getName());
        myHolder.maxRateNickname.setText(auctionItem.getMax());
        myHolder.rate.setText(auctionItem.getRate());
        myHolder.yourRate.setText("");
        myHolder.makeRateBtn.setOnClickListener(v -> {
            baseFragment.showProgressBar(R.string.wait);
            try {
                float credits = Integer.parseInt(myHolder.yourRate.getText().toString());
                baseFragment.getNetwork()
                        .pasteRate(auction.getReloadToken(), auctionItem.getLootId(),auctionItem.getItemId(), credits)
                        .subscribe(this::completePasteRate, this::error);
            } catch (Throwable throwable) {
                error(throwable);
            }
        });
    }

    private void completePasteRate(Connection.Response response) {
        ((AuctionFragment)baseFragment).completePasteRate(response);
    }

    private void error(Throwable throwable) {
        baseFragment.showSnackBar(R.string.error);
    }

    @Override
    public int getItemCount() {
        return auction.getAuctionItems().size();
    }

    public static class AuctionItemViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        TextView maxRateNickname;
        TextView rate;
        EditText yourRate;
        Button makeRateBtn;

        public AuctionItemViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            maxRateNickname = (TextView) itemView.findViewById(R.id.max_rate_nickname);
            rate = (TextView) itemView.findViewById(R.id.rate);
            yourRate = (EditText) itemView.findViewById(R.id.your_rate);
            makeRateBtn = (Button) itemView.findViewById(R.id.make_rate_btn);
        }
    }
}

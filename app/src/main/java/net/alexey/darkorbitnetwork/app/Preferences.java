package net.alexey.darkorbitnetwork.app;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    private static final String APP_PREFERENCES = "APP_PREFERENCES";

    private SharedPreferences sharedPreferences;

    public Preferences(Context context) {
        sharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }
}
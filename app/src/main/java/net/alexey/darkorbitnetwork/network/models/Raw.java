package net.alexey.darkorbitnetwork.network.models;


public class Raw {
    private int count;
    private int maxCount;

    public Raw(int count, int maxCount) {
        this.count = count;
        this.maxCount = maxCount;
    }

    public int getCount() {
        return count;
    }

    public int getMaxCount() {
        return maxCount;
    }
}

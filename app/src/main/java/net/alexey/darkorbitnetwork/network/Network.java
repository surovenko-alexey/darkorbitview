package net.alexey.darkorbitnetwork.network;


import net.alexey.darkorbitnetwork.network.models.Auction;
import net.alexey.darkorbitnetwork.network.models.AuctionItem;
import net.alexey.darkorbitnetwork.network.models.Raw;
import net.alexey.darkorbitnetwork.network.models.Skilab;
import net.alexey.darkorbitnetwork.network.models.UserData;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Network {
    private final static String USERAGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.137 YaBrowser/17.4.1.919 Yowser/2.5 Safari/537.36";
    private Map<String, String> cookie;
    private String server;

    public void setCookie(Map<String, String> cookie) {
        this.cookie = cookie;
    }

    private Connection.Response loginQuery(String username, String password) {
        try {
            Document mainPageDocument = Jsoup.connect("https://www.darkorbit.com").get();
            String actionUrl = mainPageDocument.select("form[name='bgcdw_login_form']").attr("action");
            Connection loginConnection = Jsoup.connect(actionUrl)
                    .data("username", username)
                    .data("password", password)
                    .method(Connection.Method.POST)
                    .userAgent(USERAGENT);
            return loginConnection.execute();
        } catch (IOException e) {
            return null;
        }
    }

    public Observable<Connection.Response> login(String username, String password) {
        return Observable.create(
                (Observable.OnSubscribe<Connection.Response>) subscriber -> {
                    subscriber.onNext(loginQuery(username, password));
                    subscriber.onCompleted();
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private UserData parseUserInfoQuery(Document document) {
        String nickname = document.select("div#userInfoSheet div.userInfoLine").first().text().substring(5);
        String position = null;
        String point = null;
        Elements top = document.select("table.homeRankingTable tr");
        for (Element element : top) {
            if (element.select("td").first().text().contains(nickname)) {
                position = element.select("td").get(2).text();
                point = element.select("td").get(3).text();
            }
        }
        String company = document.select("div#companyLogo").attr("class");
        company = company.substring(company.length() - 3).toUpperCase();
        server = document.baseUri().substring(8);
        server = server.substring(0, server.indexOf('.'));
        return new UserData(
                document.select("div#header_top_level span").text(),
                document.select("div#header_top_exp span").text(),
                document.select("div#header_top_hnr span").text(),
                nickname,
                company,
                document.select("div#userInfoSheet div.userInfoLine").get(2).text().substring(6),
                document.select("a#header_uri").text(),
                document.select("div#header_credits").text(),
                position,
                point,
                document.select("div#userInfoSheet div.userInfoLine").get(2).select("img").attr("src"),
                document.select("div#homeUserContent img").attr("src"));
    }

    public Observable<UserData> parseUserInfo(Document document) {
        return Observable.create(
                (Observable.OnSubscribe<UserData>) subscriber -> {
                    subscriber.onNext(parseUserInfoQuery(document));
                    subscriber.onCompleted();
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private Auction parseAuctionItemsQuery() {
        try {
            Document document = Jsoup
                    .connect("https://" + server + ".darkorbit.com/indexInternal.es?action=internalAuction")
                    .cookies(cookie)
                    .userAgent(USERAGENT)
                    .get();
            return parseAuctionItemsQuery(document);
        } catch (IOException e) {
            return null;
        }
    }

    private Auction parseAuctionItemsQuery(Document document) {
        List<AuctionItem> auctionItems = new ArrayList<>();
        Element element = document.select("script[type=\"text/javascript\"]").get(38);
        String infoText = element.childNode(0).toString().substring(14);
        infoText = infoText.substring(0,infoText.indexOf("'"));
        String scriptText = document.select("script[language=\"javascript\"]").last().childNode(0).toString();
        int index = scriptText.indexOf("reloadToken");
        String reloadToken = scriptText.substring(index + 15, index + 47);

        Elements items = document.select("tbody.auction_item_wrapper").first().select("tr[onclick=\"Auction.showItem(this)\"]");
        for (Element item : items) {
            auctionItems.add(new AuctionItem(
                    item.select("td.firstColumn img").attr("src"),
                    item.select("td.firstColumn img").attr("alt"),
                    item.select("td.auction_item_highest").text(),
                    item.select("td.auction_item_current").text(),
                    item.select("td.auction_item_instant input").get(4).attr("value"),
                    item.attr("itemKey")));
        }
        return new Auction(reloadToken, auctionItems, infoText);
    }

    public Observable<Auction> parseAuction() {
        return Observable.create(
                (Observable.OnSubscribe<Auction>) subscriber -> {
                    subscriber.onNext(parseAuctionItemsQuery());
                    subscriber.onCompleted();
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Auction> parseAuction(Document document) {
        return Observable.create(
                (Observable.OnSubscribe<Auction>) subscriber -> {
                    subscriber.onNext(parseAuctionItemsQuery(document));
                    subscriber.onCompleted();
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private Connection.Response pasteRateQuery(String reloadToken, String lootId, String itemId, float credits) {
        try {
            Connection pasteRateConnection = Jsoup.connect("https://" + server + ".darkorbit.com/indexInternal.es?action=internalAuction&reloadToken=" + reloadToken)
                    .data("reloadToken", reloadToken)
                    .data("auctionType", "hour")
                    .data("subAction", "bid")
                    .data("lootId", lootId)
                    .data("itemId", itemId)
                    .data("credits", String.valueOf(credits))
                    .data("auction_buy_button", "ПОСТАВИТЬ")
                    .cookies(cookie)
                    .method(Connection.Method.POST)
                    .userAgent(USERAGENT);
            return pasteRateConnection.execute();
        } catch (IOException e) {
            return null;
        }
    }

    public Observable<Connection.Response> pasteRate(String reloadToken, String lootId, String itemId, float credits) {
        return Observable.create(
                (Observable.OnSubscribe<Connection.Response>) subscriber -> {
                    subscriber.onNext(pasteRateQuery(reloadToken, lootId, itemId, credits));
                    subscriber.onCompleted();
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private Raw rawParse(Element element, String countQuery, String maxCountQuery) {
        String s = element.select(countQuery).text();
        s = s.replaceAll("[^0-9]", "");
        int count = Integer.parseInt(s);
        s = element.select(maxCountQuery).text();
        s = s.replaceAll("[^0-9]", "");
        return new Raw(count, Integer.parseInt(s));
    }

    private Skilab parseSkilabQuery() {
        try {
            Document skilabDocument = Jsoup.connect("https://"+server+".darkorbit.com/indexInternal.es?action=internalSkylab")
                    .cookies(cookie)
                    .userAgent(USERAGENT)
                    .get();
            Element rawElement = skilabDocument.select("#skylab").first();
            Skilab skilab = new Skilab();
            skilab.setPrometium(rawParse(rawElement, "#view_prometium_stock", "#view_prometium_maximal"));
            skilab.setEndurium(rawParse(rawElement, "#view_endurium_stock", "#view_endurium_maximal"));
            skilab.setTerbium(rawParse(rawElement, "#view_terbium_stock", "#view_terbium_maximal"));
            skilab.setPrometid(rawParse(rawElement, "#view_prometid_stock", "#view_prometid_maximal"));
            skilab.setDuranium(rawParse(rawElement, "#view_duranium_stock", "#view_duranium_maximal"));
            skilab.setXenomit(rawParse(rawElement, "#view_xenomit_stock", "#view_xenomit_maximal"));
            skilab.setPromerium(rawParse(rawElement, "#view_promerium_stock", "#view_promerium_maximal"));
            skilab.setSeprom(rawParse(rawElement, "#view_seprom_stock", "#view_seprom_maximal"));
            return skilab;
        } catch (IOException e) {
            return null;
        }
    }

    public Observable<Skilab> parseSkilab() {
        return Observable.create(
                (Observable.OnSubscribe<Skilab>) subscriber -> {
                    subscriber.onNext(parseSkilabQuery());
                    subscriber.onCompleted();
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}

package net.alexey.darkorbitnetwork.network.models;


public class Skilab {
    private Raw prometium;
    private Raw endurium;
    private Raw terbium;
    private Raw prometid;
    private Raw duranium;
    private Raw xenomit;
    private Raw promerium;
    private Raw seprom;

    public Skilab() {
    }

    public Skilab(Raw prometium, Raw endurium, Raw terbium, Raw prometid, Raw duranium, Raw xenomit, Raw promerium, Raw seprom) {
        this.prometium = prometium;
        this.endurium = endurium;
        this.terbium = terbium;
        this.prometid = prometid;
        this.duranium = duranium;
        this.xenomit = xenomit;
        this.promerium = promerium;
        this.seprom = seprom;
    }

    public Raw getPrometium() {
        return prometium;
    }

    public void setPrometium(Raw prometium) {
        this.prometium = prometium;
    }

    public Raw getEndurium() {
        return endurium;
    }

    public void setEndurium(Raw endurium) {
        this.endurium = endurium;
    }

    public Raw getTerbium() {
        return terbium;
    }

    public void setTerbium(Raw terbium) {
        this.terbium = terbium;
    }

    public Raw getPrometid() {
        return prometid;
    }

    public void setPrometid(Raw prometid) {
        this.prometid = prometid;
    }

    public Raw getDuranium() {
        return duranium;
    }

    public void setDuranium(Raw duranium) {
        this.duranium = duranium;
    }

    public Raw getXenomit() {
        return xenomit;
    }

    public void setXenomit(Raw xenomit) {
        this.xenomit = xenomit;
    }

    public Raw getPromerium() {
        return promerium;
    }

    public void setPromerium(Raw promerium) {
        this.promerium = promerium;
    }

    public Raw getSeprom() {
        return seprom;
    }

    public void setSeprom(Raw seprom) {
        this.seprom = seprom;
    }
}

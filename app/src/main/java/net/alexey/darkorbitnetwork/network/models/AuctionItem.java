package net.alexey.darkorbitnetwork.network.models;


public class AuctionItem {
    private String imgUrl;
    private String name;
    private String max;
    private String rate;
    private String lootId;
    private String itemId;

    public AuctionItem(String imgUrl, String name, String max, String rate, String lootId, String itemId) {
        this.imgUrl = imgUrl;
        this.name = name;
        this.max = max;
        this.rate = rate;
        this.lootId = lootId;
        this.itemId = itemId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public String getName() {
        return name;
    }

    public String getMax() {
        return max;
    }

    public String getRate() {
        return rate;
    }

    public String getLootId() {
        return lootId;
    }

    public String getItemId() {
        return itemId;
    }
}

package net.alexey.darkorbitnetwork.network.models;


public class UserData {
    private String lvl;
    private String experience;
    private String honor;
    private String nickname;
    private String company;
    private String rank;
    private String uridium;
    private String credits;
    private String position;
    private String point;
    private String rankUrl;
    private String avatarUrl;

    public UserData(String lvl, String experience, String honor, String nickname, String company,
                    String rank, String uridium, String credits, String position, String point,
                    String rankUrl, String avatarUrl) {
        this.lvl = lvl;
        this.experience = experience;
        this.honor = honor;
        this.nickname = nickname;
        this.company = company;
        this.rank = rank;
        this.uridium = uridium;
        this.credits = credits;
        this.position = position;
        this.point = point;
        this.rankUrl = rankUrl;
        this.avatarUrl = avatarUrl;
    }

    public String getLvl() {
        return lvl;
    }

    public String getExperience() {
        return experience;
    }

    public String getHonor() {
        return honor;
    }

    public String getNickname() {
        return nickname;
    }

    public String getCompany() {
        return company;
    }

    public String getRank() {
        return rank;
    }

    public String getUridium() {
        return uridium;
    }

    public String getCredits() {
        return credits;
    }

    public String getPosition() {
        return position;
    }

    public String getPoint() {
        return point;
    }

    public String getRankUrl() {
        return rankUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}

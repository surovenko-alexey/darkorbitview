package net.alexey.darkorbitnetwork.network.models;


import java.util.List;

public class Auction {
    String reloadToken;
    List<AuctionItem> auctionItems;
    String message;

    public Auction(String reloadToken, List<AuctionItem> auctionItems, String message) {
        this.reloadToken = reloadToken;
        this.auctionItems = auctionItems;
        this.message = message;
    }

    public String getReloadToken() {
        return reloadToken;
    }

    public List<AuctionItem> getAuctionItems() {
        return auctionItems;
    }

    public String getMessage() {
        return message;
    }
}
